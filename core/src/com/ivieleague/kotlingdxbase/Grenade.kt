package com.ivieleague.kotlingdxbase

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.ivieleague.kotlen.behaviors.AlarmBehavior
import com.ivieleague.kotlen.entity.Entity
import com.ivieleague.kotlen.physics.*

/**
 * Created by joseph on 7/17/15.
 */
public class Grenade(team: Int, color: Color, startPosition: Vector2, startVelocity: Vector2, strength: Float, time: Float) : Entity<TankGame>() {


    public constructor(tank: Tank, startVelocity: Vector2)
    : this(tank.team, tank.color, tank.module.body!!.position, startVelocity, 50f, 2f) {
    }

    private val strength: Float = strength
    private val myShape: PolygonShape = PolygonShape()
    private val color: Color = color
    private val team: Int = team
    public val module: Box2DModule = Box2DModule({
        myShape.setAsBox(.25f, .25f)
        val body = makeBody {
            type = BodyDef.BodyType.DynamicBody
            position.set(startPosition)
            linearVelocity.set(startVelocity)
            bullet = true
        }.addFixture {
            shape = myShape
            density = 1f
            filter.groupIndex = (-team).toShort()
            restitution = .5f
        }
        body
    }, {
        myShape.dispose()
    },
            entity = this)

    private val explosionAlarm = AlarmBehavior.Alarm(time) {
        if (owner != null) {
            val owner = owner!!
            owner.entityBehavior.remove(this@Grenade)

            val explosionCenter = module.body!!.position

            for (module in owner.physicsBehavior.set) {
                if (module.body == null
                        || module.body?.getType() != BodyDef.BodyType.DynamicBody
                        || module == this.module) continue
                val body = module.body!!

                val dx = body.x - explosionCenter.x
                val dy = body.y - explosionCenter.y

                var dist = dx * dx + dy * dy
                if (dist < .25f) {
                    dist = 1f
                }
                val scale = strength / dist
                body.applyLinearImpulse(
                        dx * scale,
                        dy * scale,
                        body.x,
                        body.y,
                        true
                )
            }
        }

    }

    override fun create(owner: TankGame) {
        super.create(owner)
        addBehavior(owner.physicsBehavior, module)
        addBehavior(owner.shapeBehavior, {
            if (module.body != null) {
                val body = module.body!!
                it.setColor(color)
                body.getTransformMatrix(it.getTransformMatrix())
                it.updateMatrices()
                it.rect(-.25f, -.25f, .5f, .5f)
            }
        })
        addBehavior(owner.get({ AlarmBehavior(0) }), explosionAlarm)
    }
}