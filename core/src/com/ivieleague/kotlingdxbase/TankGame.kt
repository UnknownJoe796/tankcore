package com.ivieleague.kotlingdxbase

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Box2D
import com.badlogic.gdx.physics.box2d.ChainShape
import com.ivieleague.kotlen.behaviors.BehaviorSet
import com.ivieleague.kotlen.entity.EntityBehavior
import com.ivieleague.kotlen.language.addPolar
import com.ivieleague.kotlen.physics.Box2DBehavior
import com.ivieleague.kotlen.physics.Box2DModule
import com.ivieleague.kotlen.physics.addFixture
import com.ivieleague.kotlen.physics.makeBody
import com.ivieleague.kotlen.rendering.ShapeRendererBehavior
import kotlin.properties.Delegates

/**
 * Created by joseph on 7/17/15.
 */

public class TankGame : BehaviorSet() {
    public var resources: Resources by Delegates.notNull()
    public val camera: OrthographicCamera = OrthographicCamera()
    public val worldSize: Vector2 = Vector2(30f, 22f);

    public val physicsBehavior: Box2DBehavior = Box2DBehavior(0)
    public val shapeBehavior: ShapeRendererBehavior = ShapeRendererBehavior(1, camera)
    public val lineBehavior: ShapeRendererBehavior = ShapeRendererBehavior(1, camera, false)
    public val entityBehavior: EntityBehavior<TankGame> = EntityBehavior(-1, this)

    init {
        Box2D.init();
        add(physicsBehavior)
        add(shapeBehavior)
        add(lineBehavior)
        add(entityBehavior)

        val colors = arrayListOf(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW)
        for (index in 0..3) {
            val tank = Tank(
                    index,
                    colors[index],
                    worldSize
                            .cpy()
                            .scl(.5f)
                            .addPolar(
                                    worldSize.y * .25f,
                                    Math.PI * .5 * index
                            )
            )
            entityBehavior.add(tank)
        }

        for (i in 0..25) {
            entityBehavior.add(SquareJunkEntity())
        }

        //make border
        val borderCoords = floatArrayOf(
                0f, 0f,
                worldSize.x, 0f,
                worldSize.x, worldSize.y,
                0f, worldSize.y
        )
        val cShape: ChainShape = ChainShape();
        cShape.createLoop(borderCoords)

        physicsBehavior.add(Box2DModule({
            makeBody {}.addFixture {
                shape = cShape
            }
        }, {
            cShape.dispose()
        }))

        lineBehavior.add({
            it.setColor(1f, 1f, 1f, 1f)
            it.polygon(borderCoords)
        })
    }

    override fun create() {

        resources = Resources()

        super.create()
    }

    override fun dispose() {
        resources.dispose()
        super.dispose()
    }

    override fun render() {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        super.render()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        camera.setToOrtho(false, worldSize.x, worldSize.x * height / width)
        camera.position.set(worldSize.cpy().scl(.5f), 0f)
        camera.update(true)
    }

    class Resources {
        public fun dispose() {
        }
    }

}
