package com.ivieleague.kotlingdxbase;

import com.badlogic.gdx.ApplicationAdapter;
import com.ivieleague.kotlen.behaviors.BehaviorSet;

public class KotlinGdxBaseGame extends ApplicationAdapter {
	BehaviorSet set;

	@Override
	public void create () {
        set = new TankGame();
        set.create();
	}

	@Override
	public void render () {
        set.render();
	}

    @Override
    public void resize(int width, int height) {
        set.resize(width, height);
    }

    @Override
    public void pause() {
        set.pause();
    }

    @Override
    public void resume() {
        set.resume();
    }

    @Override
    public void dispose() {
        set.dispose();
    }
}
