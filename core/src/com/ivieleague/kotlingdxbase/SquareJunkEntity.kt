package com.ivieleague.kotlingdxbase

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.ivieleague.kotlen.behaviors.SimpleSetBehavior
import com.ivieleague.kotlen.entity.Entity
import com.ivieleague.kotlen.physics.Box2DModule
import com.ivieleague.kotlen.physics.addFixture
import com.ivieleague.kotlen.physics.getTransformMatrix
import com.ivieleague.kotlen.physics.makeBody

/**
 * Created by joseph on 7/17/15.
 */

public class SquareJunkEntity : Entity<TankGame>() {
    private val myShape: PolygonShape = PolygonShape()
    public val module: Box2DModule = Box2DModule({
        myShape.setAsBox(.5f, .5f)
        val body = makeBody {
            type = BodyDef.BodyType.DynamicBody
            position.set(
                    Math.random().toFloat() * (owner?.worldSize?.x ?: 0f),
                    Math.random().toFloat() * (owner?.worldSize?.y ?: 0f)
            )
            linearVelocity.set(
                    Math.random().toFloat() * 10 - 5,
                    Math.random().toFloat() * 10 - 5
            )
        }.addFixture {
            shape = myShape
            density = 1f
        }
        body
    }, {
        myShape.dispose()
    },
            entity = this)

    override fun create(owner: TankGame) {
        super.create(owner)
        val color = Color(Math.random().toFloat(), Math.random().toFloat(), Math.random().toFloat(), 1f)
        addBehavior(owner.physicsBehavior, module)
        addBehavior(owner.shapeBehavior, {
            if (module.body != null) {
                val body = module.body!!
                it.setColor(color)
                body.getTransformMatrix(it.getTransformMatrix())
                it.updateMatrices()
                it.rect(-.5f, -.5f, 1f, 1f)
            }
        })
        addBehavior(owner.get("BallEntity.attract") {
            SimpleSetBehavior<SquareJunkEntity>(5, onRender = {
                if (Gdx.input.isTouched() && it.module.body != null) {
                    val body = it.module.body!!;
                    val worldLocation = it.owner!!.camera.unproject(Vector3(Gdx.input.getX().toFloat(), Gdx.input.getY().toFloat(), 0f))
                    val delta = body.getPosition().cpy().sub(worldLocation.x, worldLocation.y).scl(-1f)
                    var power = 500f / delta.len2()
                    if (power > 500f) power = 500f
                    body.applyForceToCenter(delta.setLength(power), true)
                }
            })
        }, this)
    }
}