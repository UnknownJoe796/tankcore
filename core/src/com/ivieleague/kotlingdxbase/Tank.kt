package com.ivieleague.kotlingdxbase

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.CircleShape
import com.ivieleague.kotlen.entity.Entity
import com.ivieleague.kotlen.language.XBox360Pad
import com.ivieleague.kotlen.physics.*

/**
 * Created by joseph on 7/17/15.
 */
public class Tank(controllerIndex: Int, team: Int, color: Color, startPosition: Vector2) : Entity<TankGame>() {
    public constructor(controllerIndex: Int, color: Color, startPosition: Vector2)
    : this(controllerIndex, controllerIndex + 1, color, startPosition)

    public val startPosition: Vector2 = startPosition;
    public val color: Color = color
    public val controllerIndex: Int = controllerIndex
    public val team: Int = team
    public val radius: Float = .5f

    private val myShape: CircleShape = CircleShape()
    public val module: Box2DModule = Box2DModule({
        myShape.setRadius(radius)
        val body = makeBody {
            type = BodyDef.BodyType.DynamicBody
            position.set(startPosition)
            linearDamping = .5f
        }.addFixture {
            shape = myShape
            density = 1f
            filter.groupIndex = (-team).toShort()
        }
        body
    }, {
        myShape.dispose()
    },
            entity = this)

    override fun create(owner: TankGame) {
        super.create(owner)
        addBehavior(owner.physicsBehavior, module)
        addBehavior(owner.shapeBehavior, {
            if (module.body != null) {
                val body = module.body!!
                body.getTransformMatrix(it.getTransformMatrix())
                it.updateMatrices()
                it.setColor(color)
                it.circle(0f, 0f, radius, 12)
                it.setColor(Color.WHITE)
                it.circle(0f, 0f, radius / 2, 12)
            }
        })
        setupMoveBehavior()
        setupShootBehavior()
        setupShootGrenadeBehavior()
    }

    private fun setupMoveBehavior() {
        addBehavior(owner!!, "Tank.move", this, 0, onRender = {
            val MOVE_FORCE = 100f
            val MOVE_SPEED = 10f
            val controller = XBox360Pad.opt(controllerIndex);
            if (controller != null) {
                module.body?.applyForceToCenter(controller.left.scl(MOVE_FORCE), true)
            }
            val vel = module.body?.velocity
            if (vel != null && vel.len2() > MOVE_SPEED * MOVE_SPEED) {
                module.body?.velocity = vel.setLength2(MOVE_SPEED * MOVE_SPEED)
            }
        })
    }

    private var cooldown: Float = 0f
    private fun setupShootBehavior() {

        addBehavior(owner!!, "Tank.shoot", this, 0, onRender = {
            val SHOT_COOLDOWN = .2f
            val SHOT_SPEED = 100f

            if (cooldown > 0f) {
                cooldown -= Gdx.graphics.getDeltaTime()
            } else {

                val controller = XBox360Pad.opt(controllerIndex);
                if (controller != null) {
                    if (!controller.rb && controller.right.len2() > .125f) {
                        owner?.entityBehavior?.add(Bullet(this, controller.right.scl(SHOT_SPEED)))
                        cooldown = SHOT_COOLDOWN
                    }
                }
            }
        })
    }

    private fun setupShootGrenadeBehavior() {

        addBehavior(owner!!, "Tank.grenade", this, 0, onRender = {
            val SHOT_COOLDOWN = 2f
            val SHOT_SPEED = 20f

            if (cooldown > 0f) {
                cooldown -= Gdx.graphics.getDeltaTime()
            } else {

                val controller = XBox360Pad.opt(controllerIndex);
                if (controller != null) {
                    if (controller.rb && controller.right.len2() > .125f) {
                        owner?.entityBehavior?.add(Grenade(this, controller.right.scl(SHOT_SPEED)))
                        cooldown = SHOT_COOLDOWN
                    }
                }
            }
        })
    }
}